syntax on
set mouse-=a

set number
set nu
set nohlsearch
set hidden
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set noerrorbells
set incsearch
set scrolloff=4
colorscheme ron
