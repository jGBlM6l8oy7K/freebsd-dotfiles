function fish_greeting
	fortune
end

funcsave fish_greeting

function fish_right_prompt
	date '+%H:%M:%S %m/%d/%Y'
end

funcsave fish_right_prompt
