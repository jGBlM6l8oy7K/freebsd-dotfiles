#!/bin/sh

# $1 = i3status.conf

wip() {
	IP=$(wget http://ipinfo.io/ip -qO -)
	if [ "$IP" == "" ]; then
		IP=':C'
	fi
}

layout() {
	case "$(xset -q|grep LED|awk '{print $10}')" in
		"00000000") KBD="US" ;;
		"00001000") KBD="HU" ;;
		*) KBD="??" ;;
	esac
}

i3status -c "$1" | while :
do
	read line
	layout
	echo "$line |  $KBD | " || exit 1

done
